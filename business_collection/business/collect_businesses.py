import yaml
import requests

import numpy as np

from PIL import Image
from io import BytesIO
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.alert import Alert


class CollectBusinesses:

    def __init__(self, businesses_id, config_path):
        self.businesses_id = businesses_id

        with open(config_path, 'r') as yml:
            self.config = yaml.load(yml, Loader=yaml.FullLoader)

        # create the driver
        self.driver = self.__create_driver()

    def __create_driver(self):
        driver_path = self.config["driver"]["chrome"]
        headless = self.config["driver"]["headless"]

        options = webdriver.ChromeOptions()
        if headless:
            options.add_argument("--headless")
        driver = webdriver.Chrome(driver_path, options=options)

        return driver

    def start(self):

        # open the link for collecting business id
        url = self.config["urls"]["get_business"] + self.config["urls"]["get_business_target"]
        self.driver.get(url)

        # convert the website into english language
        cvt_english = self.driver.find_element_by_link_text("In English")
        cvt_english.send_keys(u'\ue007')

        # get the list of businesses
        businesses_details = self.__get_businesses()

        self.driver.close()

        return businesses_details

    def __get_businesses(self):
        businesses_details = []
        for idx, business in enumerate(self.businesses_id):
            # get the business id and time of registration
            bid, reg_time = business

            # get location for adding business id
            field_id = "_ctl0_cphSisalto_ytunnus"
            field = self.driver.find_element_by_id(field_id)
            field.clear()
            field.send_keys(bid)

            # press enter to search the data
            field.send_keys(Keys.ENTER)

            # check if data is available or not
            try:
                # the data is not available, so handle the alert and move to next id
                alert = Alert(self.driver)
                alert.accept()
                continue
            except:
                # parse the page source page into html form
                source = BeautifulSoup(self.driver.page_source)

            # get the first row from the resultant list
            table = source.find("table")
            row = table.find_all("tr")[1]
            bid = row.find_all("td")[0].get_text()
            bid = self.__clean_text(bid)

            # redirect to page having business details
            blink = self.driver.find_element_by_link_text(bid)
            blink.send_keys(u'\ue007')

            # store the information
            info = {
                "Business ID": bid,
                "Business URL": self.driver.current_url
            }

            soup = BeautifulSoup(self.driver.page_source)
            tb = soup.find("table")
            for rw in tb.find_all("tr"):
                cw = rw.find_all("td")
                if len(cw):
                    key, value = cw[0].get_text(), cw[1].get_text()
                    key, value = self.__clean_text(key), self.__clean_text(value)

                    if key == "E-mail":
                        img = rw.find("img")
                        email_src = self.config["urls"]["get_business"] + img["src"]
                        email_web = requests.get(email_src)

                        email_img = Image.open(BytesIO(email_web.content))
                        info["E-mail"] = email_img
                    else:
                        info[key] = value

            info["Time of registration"] = reg_time

            blink = self.driver.find_element_by_link_text("Back")
            blink.send_keys(u'\ue007')

            businesses_details.append(info)

        return businesses_details

    @staticmethod
    def __clean_text(text):
        words = text.split("\n")
        words = [word.strip() for word in words if len(word.strip()) > 1]
        text = " ".join(words)

        return text

