import os
import cv2
import yaml
import pytesseract
import mysql.connector

import numpy as np

from mysql.connector import Error
from datetime import datetime


# pytesseract.pytesseract.tesseract_cmd = r'C:/Program Files/Tesseract-OCR/tesseract.exe'


class StoreBusinesses:

    def __init__(self, businesses_details, config_path):
        self.businesses_details = businesses_details

        with open(config_path, 'r') as yml:
            self.config = yaml.load(yml, Loader=yaml.FullLoader)

        self.connection = self.create_connection()

    def start(self):

        # path for storing emails
        email_store = self.config["path"]["store"]
        if not os.path.exists(email_store):
            os.makedirs(email_store)

        # store the emails in output folder=
        for idx, b_detail in enumerate(self.businesses_details):
            if "E-mail" in b_detail:
                if len(str(b_detail["E-mail"])):
                    email_path = os.path.join(email_store, b_detail["Business ID"]+".png")
                    email_img = b_detail["E-mail"]
                    email_img.save(email_path)
                    email_img = self.__clean_image__(email_img)

                    self.businesses_details[idx]["E-mail"] = self.__extract_text__(email_img)

        for idx, b_detail in enumerate(self.businesses_details):
            if "Business ID" not in b_detail:
                continue

            b_id = b_detail["Business ID"]
            b_url = b_detail["Business URL"]
            b_name = b_detail["Company name"] if "Company name" in b_detail else "Unnamed"

            b_tel = b_detail["Telephone"] if "Telephone" in b_detail else ""
            b_mob = b_detail["Mobile Phone"] if "Mobile Phone" in b_detail else ""

            if "E-mail" in b_detail:
                b_mail = b_detail["E-mail"]
                b_mail_url = "emails/"+str(b_id)+".png"
            else:
                b_mail = ""
                b_mail_url = ""

            b_reg_tm = str(datetime.strptime(b_detail["Time of registration"], "%d.%m.%Y %H:%M"))

            value = str((b_id, b_url, b_name, b_tel, b_mob, b_mail, b_mail_url, b_reg_tm))

            create_business = f"""
                INSERT INTO `businesses_business` 
                (`business_id`, `business_url`, `company_name`, `telephone`, `mobile`, `email_id`, `email_img`, `registration_time`)
                VALUES {value};
            """

            self.execute_query(create_business)

    def execute_query(self, query):
        cursor = self.connection.cursor()
        try:
            cursor.execute(query)
            self.connection.commit()
            print("Query executed successfully")
        except Error as e:
            print(f"The error '{e}' occurred")

    def create_connection(self):
        db_connection = None
        try:
            db_connection = mysql.connector.connect(
                host=self.config["mysql"]["host_name"],
                user=self.config["mysql"]["user_name"],
                passwd=self.config["mysql"]["user_password"],
                database=self.config["mysql"]["db_name"]
            )
            print("Connection to MySQL DB successful")
        except Error as e:
            print(f"The error '{e}' occurred")

        return db_connection

    @staticmethod
    def __clean_image__(image):
        image = np.array(image)
        h, w = image.shape[:2]  # get image height and width

        # create extra outer space
        background = image.copy()
        background.fill(255)
        background = cv2.resize(background, (w + 6, h + 6), interpolation=cv2.INTER_CUBIC)

        margin = 3
        background[margin:margin + h, margin:margin + w] = image

        # image process
        zoom_in = cv2.resize(background, (0, 0), fx=4, fy=4, interpolation=cv2.INTER_CUBIC)
        _, binary = cv2.threshold(zoom_in, 127, 255, cv2.THRESH_BINARY)
        dilate = cv2.erode(binary, kernel=(3, 3), iterations=1)
        zoom_out = cv2.resize(dilate, (0, 0), fx=0.6, fy=0.6, interpolation=cv2.INTER_AREA)
        _, binary = cv2.threshold(zoom_out, 127, 255, cv2.THRESH_BINARY)

        return binary

    @staticmethod
    def __extract_text__(image):
        data = pytesseract.image_to_data(image, config='--oem 3 --psm 6', output_type='data.frame', lang="fra+eng")
        data = data[data.conf != -1]

        text = ""
        if not data.empty:
            data["text"] = data["text"].apply(str)
            text = " ".join(data["text"].values.tolist())

        text = ".".join(text.split(" "))
        text = "".join([char for char in list(text) if char.isalnum() or (char in [".", "_", "-", "@"])])

        return text

