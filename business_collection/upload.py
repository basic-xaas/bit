import os
import yaml

import numpy as np
import pandas as pd

from PIL import Image
from datetime import datetime
from business import StoreBusinesses


if __name__ == '__main__':
    config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.yml')
    with open(config_path, 'r') as yml:
        config = yaml.load(yml, Loader=yaml.FullLoader)

    old_data_path = config["path"]["old_data"]
    folders = os.listdir(old_data_path)
    for folder in folders:
        op_path = os.path.join(old_data_path, folder)
        full_data = pd.read_excel(os.path.join(op_path, "business.xlsx"))
        email_data = pd.read_excel(os.path.join(op_path, "business_info.xlsx"))
        data = pd.merge(full_data, email_data, on="Business ID", how="outer")
        data = data.replace(np.nan, "")
        records = np.array_split(data, int(data.shape[0]/500))

        for sec in records:
            businesses_detail = []
            for idx, rc in sec.iterrows():
                print(f"#{idx+1} Loading Record, Business ID:{rc['Business ID']}")
                email_path = os.path.join(op_path, "emails", rc["Business ID"] + ".png")
                if os.path.exists(email_path):
                    email = Image.open(email_path)
                else:
                    email = ""

                businesses_detail.append({
                    "Business ID": rc["Business ID"],
                    "Business URL": "",
                    "Company name": rc["Company name"],
                    "Telephone": rc["Telephone_x"],
                    "Mobile Phone": rc["Mobile Phone_x"],
                    "E-mail": email,
                    "Time of registration": datetime.strptime(rc["Time of registration"], "%Y-%m-%d %H:%M:%S").strftime("%d.%m.%Y %H:%M")
                })

            StoreBusinesses(businesses_detail, config_path).start()
        print("Folder Completed:", folder, "\n\n")
