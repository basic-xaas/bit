import numpy as np
import datetime

from celery import shared_task
from time import sleep

from .business import CollectId, CollectBusinesses, StoreBusinesses


def divide_to_chunks(data, num):
    for i in range(0, len(data), num):
        yield data[i:i + num]


@shared_task()
def sleepy(duration):
    sleep(duration)

    return None


@shared_task()
def load_businesses(start_date, end_date):
    print(f"Collecting Businesses Registered in range {start_date}-{end_date}")
    businesses_id = CollectId(start_date, end_date).start()
    # businesses_id = businesses_id[:10]
    print(f"Number of Businesses: {len(businesses_id)}")

    if len(businesses_id):

        # apply chunks on the results
        business_sets = divide_to_chunks(businesses_id, 10)

        # collect the business details
        businesses_detail = []
        for idx, b_set in enumerate(business_sets):
            businesses = CollectBusinesses(b_set).start()
            businesses_detail.append(businesses)

        businesses_detail = np.array(businesses_detail).flatten()
        StoreBusinesses(businesses_detail).start()

    return True


@shared_task()
def load_businesses_daily():
    today = datetime.date.today()
    yesterday = today - datetime.timedelta(days=1)

    start_date = yesterday.strftime("%d.%m.%Y")
    end_date = start_date

    print(f"Collecting Businesses Registered in range {start_date}-{end_date}")
    businesses_id = CollectId(start_date, end_date).start()
    # businesses_id = businesses_id[:10]
    print(f"Number of Businesses: {len(businesses_id)}")

    if len(businesses_id):

        # apply chunks on the results
        business_sets = divide_to_chunks(businesses_id, 10)

        # collect the business details
        businesses_detail = []
        for idx, b_set in enumerate(business_sets):
            businesses = CollectBusinesses(b_set).start()
            businesses_detail.append(businesses)

        businesses_detail = np.array(businesses_detail).flatten()
        StoreBusinesses(businesses_detail).start()

    return True
