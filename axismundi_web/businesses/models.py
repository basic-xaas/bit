from django.db import models


class Business(models.Model):
    business_id = models.CharField(unique=True, max_length=20)
    business_url = models.URLField(max_length=128)
    company_name = models.CharField(max_length=60)
    telephone = models.CharField(max_length=30)
    mobile = models.CharField(max_length=30)
    email_id = models.CharField(max_length=60)
    email_img = models.ImageField()
    registration_time = models.DateTimeField()

    def __str__(self):
        return self.business_id
