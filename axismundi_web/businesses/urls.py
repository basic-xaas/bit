from django.urls import path
from django.views.generic import TemplateView

from . import views

app_name = 'businesses'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('businesses_excel', views.BusinessesExcelView.as_view(), name='businesses_excel'),
    path('businesses_load', views.BusinessesLoadView.as_view(), name='load_businesses'),
    path('celery', views.CeleryView.as_view(), name='celery')
]
