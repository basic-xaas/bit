function myFunction(imgs) {
  var expandImg = document.getElementById("expandedImg");
  var imgText = document.getElementById("imgtext");
  expandImg.src = imgs.src;
  imgText.innerHTML = imgs.alt;
  expandImg.parentElement.style.display = "block";
}

$(document).ready(function() {

  $('.wrap-1').imagesLoaded( function() {
    $("#exzoom").exzoom({
          autoPlay: false,
      });
    $("#exzoom").removeClass('hidden')
  });

  $('.product_wrap .product_img').hover(function(){
      product_id = this.id
      $( ".product_wrap #"+product_id+" .img_2" ).animate({
        opacity: 1,
      }, 600, function() {
        // Animation complete.
      });
      // $(".product_wrap .product_img .img_2").css("opacity", "1")
  }, function() {
      $( ".product_wrap #"+product_id+" .img_2" ).animate({
        opacity: 0,
      }, 600, function() {
        // Animation complete.
      });
      // $(".product_wrap .product_img .img_2").css("opacity", "0")
  })


  $(window).scroll(function () {

    //console.log($(window).scrollTop())
    if($(window).width() <= 720){
      if ($(window).scrollTop() > 289) {
        $('.wrapper-3').addClass('navbar-fixed');
      }
      if ($(window).scrollTop() < 290) {
        $('.wrapper-3').removeClass('navbar-fixed');
      }
    }else if($(window).width() > 720 && $(window).width()<=1280){
      if ($(window).scrollTop() > 182) {
        $('.wrapper-3').addClass('navbar-fixed');
      }
      if ($(window).scrollTop() < 183) {
        $('.wrapper-3').removeClass('navbar-fixed');
      }

    }else{
      if ($(window).scrollTop() > 200) {
        $('.wrapper-3').addClass('navbar-fixed');
        $(".wrapper-3 .nav-item").css("padding", "0 0");
      }
      if ($(window).scrollTop() < 201) {
        $('.wrapper-3').removeClass('navbar-fixed');
        $(".wrapper-3 .nav-item").css("padding", "15px 0px");
      }
    }


  });

  // Create Account JS
  $("#signUpForm").submit(function(event){
    var csrf_token = $("input[name='csrfmiddlewaretoken']",this).val();
    var name = $("input[name='name']",this).val();
    var email = $("input[name='email']",this).val();
    var mobile = $("input[name='mobile']",this).val();
    $("#signUpMessage").hide();

    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    if(!pattern.test(email)){
      $("#signUpMessage").html("*Invalid email id, please enter valid email id");
      $("#signUpMessage").show();
      return false;
    }

    var pattern = /^[6-9]\d{9}$/i
    if(!pattern.test(mobile)){
      $("#signUpMessage").html("*Invalid mobile number, please insert valid mobile number");
      $("#signUpMessage").show();
      return false;
    }

    input = {
      csrfmiddlewaretoken: csrf_token,
      name: name,
      email: email,
      mobile: mobile,
    }

    $.post("/accounts/registeruser/", input)
      .done(function( data ) {
        //console.log(data);

        if(data == 'U/R Error'){
          $("#signUpMessage").html("*Email Id already exits, please try to LogIn");
          $("#signUpMessage").show();
          return false;
        }

        $("#signUpMessage").html("*Successfully Registered, check mail for complete verify your account");
        $("#signUpMessage").css('color', 'green');
        $("#signUpMessage").show();

        setTimeout(function(){
          location.reload(true);
        }, 5000);

    });

    return false;

  });

  // Create Account JS
  $("#logInForm").submit(function(event){
    var csrf_token = $("input[name='csrfmiddlewaretoken']",this).val();
    var email = $("input[name='email']",this).val();
    var password = $("input[name='password']",this).val();
    $("#logInMessage").hide();

    var pattern = /^\b[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}\b$/i
    if(!pattern.test(email)){
      $("#logInMessage").html("*Invalid email id, please enter valid email id");
      $("#logInMessage").show();
      return false;
    }

    input = {
      csrfmiddlewaretoken: csrf_token,
      email: email,
      password: password,
    }


    $.post("/accounts/loginuser/", input)
      .done(function( data ) {
        console.log(data);

        if(data){
          $("#logInMessage").html("*Successfully Login");
          $("#logInMessage").css('color', 'green');
          $("#logInMessage").show();
        }else{
          $("#logInMessage").html("*Invalid email or password, please try again");
          $("#logInMessage").show();
          return false;
        }

        setTimeout(function(){
          location.reload(true);
        }, 1000);

    });

    return false;

  });

  // Add to cart
  $(".add_to_cart").click(function() {
    var product = this.id;
    var csrf_token = $("input[name='csrfmiddlewaretoken']").val();

    input = {
      csrfmiddlewaretoken: csrf_token,
      product: product,
    }

    $.post("/accounts/addtocart/", input)
      .done(function( data ) {

        bootbox.alert({
            message: data+" added successfully",
            backdrop: true
        });

    });
  });

  // Remove from cart to cart
  $(".remove_from_cart").click(function() {
    var product = this.id;
    var csrf_token = $("input[name='csrfmiddlewaretoken']").val();

    input = {
      csrfmiddlewaretoken: csrf_token,
      product: product,
    }

    $.post("/accounts/removefromcart/", input)
      .done(function( data ) {

        bootbox.alert({
            message: data+" removed successfully",
            backdrop: true
        });

        setTimeout(function(){
          location.reload(true);
        }, 500);

    });
  });




});
