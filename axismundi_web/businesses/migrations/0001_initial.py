# Generated by Django 3.1.4 on 2020-12-19 16:52

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Business',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('business_id', models.CharField(max_length=20, unique=True)),
                ('business_url', models.URLField(max_length=128)),
                ('company_name', models.CharField(max_length=60)),
                ('telephone', models.CharField(max_length=15)),
                ('mobile', models.CharField(max_length=15)),
                ('email_id', models.CharField(max_length=60)),
                ('email_img', models.ImageField(upload_to='')),
                ('registration_time', models.DateTimeField()),
            ],
        ),
    ]
