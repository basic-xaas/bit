import os
import datetime
import xlsxwriter

from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.http import HttpResponse
from django.views import View
from django.http import JsonResponse
from django.conf import settings
from django.core.exceptions import EmptyResultSet
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from .models import *
from .tasks import *


class IndexView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):
        start_date = request.GET["start_date"] if "start_date" in request.GET else ""
        end_date = request.GET["end_date"] if "end_date" in request.GET else ""
        rc_num = int(request.GET["rc_num"]) if "rc_num" in request.GET else 10

        if end_date:
            yesterday = datetime.datetime.strptime(end_date, "%Y-%m-%d")
            recent_day = yesterday + datetime.timedelta(days=1)
        else:
            recent_day = datetime.date.today()

        if start_date:
            last_day = datetime.datetime.strptime(start_date, "%Y-%m-%d")
        else:
            last_day = recent_day - datetime.timedelta(days=8)

        try:
            businesses = get_list_or_404(Business.objects.order_by('-registration_time'), registration_time__range=(last_day, recent_day))
        except Exception as e:
            print("Error in collecting business details,", e)
            businesses = []

        s_date = last_day
        e_date = recent_day - datetime.timedelta(days=1)

        page = request.GET.get('page', 1)
        paginator = Paginator(businesses, rc_num)
        try:
            businesses = paginator.page(page)
        except PageNotAnInteger:
            businesses = paginator.page(1)
        except EmptyPage:
            businesses = paginator.page(paginator.num_pages)

        context = {
            'start_date': s_date.strftime("%Y-%m-%d"),
            'end_date': e_date.strftime("%Y-%m-%d"),
            'rc_num': rc_num,
            'businesses': businesses
        }

        return render(request, 'businesses/index.html', context)


class BusinessesExcelView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):
        return HttpResponse("Call POST to download Businesses in Excel")

    def post(self, request):
        # content-type of response
        response = HttpResponse(content_type='application/ms-excel')

        # decide file name
        response['Content-Disposition'] = 'attachment; filename="businesses.xlsx"'

        data = request.POST.dict()
        start_date = data["start_date"] if "start_date" in data else ""
        end_date = data["end_date"] if "end_date" in data else ""

        if end_date:
            yesterday = datetime.datetime.strptime(end_date, "%Y-%m-%d")
            recent_day = yesterday + datetime.timedelta(days=1)
        else:
            recent_day = datetime.date.today()

        if start_date:
            last_day = datetime.datetime.strptime(start_date, "%Y-%m-%d")
        else:
            last_day = recent_day - datetime.timedelta(days=8)

        try:
            businesses = get_list_or_404(Business.objects.order_by('-registration_time'), registration_time__range=(last_day, recent_day))
        except Exception as e:
            businesses = []

        if len(businesses):
            # Make Excel
            excel_path = os.path.join(settings.MEDIA_ROOT, 'temp.xlsx')
            workbook = xlsxwriter.Workbook(excel_path)
            worksheet = workbook.add_worksheet()

            worksheet.set_column(0, 0, width=12)
            worksheet.set_column(1, 1, width=40)
            worksheet.set_column(2, 2, width=15)
            worksheet.set_column(3, 3, width=20)
            worksheet.set_column(4, 4, width=35)
            worksheet.set_column(5, 5, width=35)
            worksheet.set_column(6, 6, width=22)

            cell_format = workbook.add_format()
            cell_format.set_bold()

            worksheet.write("A1", "Business ID", cell_format)
            worksheet.write("B1", "Company Name", cell_format)
            worksheet.write("C1", "Telephone", cell_format)
            worksheet.write("D1", "Mobile Phone", cell_format)
            worksheet.write("E1", "Email Text", cell_format)
            worksheet.write("F1", "Email Image", cell_format)
            worksheet.write("G1", "Time of Registration", cell_format)

            ix = 2
            for idx, business in enumerate(businesses):
                bid = business.business_id
                email_image = os.path.join(settings.MEDIA_ROOT, 'emails', str(bid) + ".png")
                reg_time = business.registration_time.strftime("%d %B, %Y %H:%S")
                if business.business_url:
                    worksheet.write_url("A" + str(ix), business.business_url, string=bid)
                else:
                    worksheet.write("A" + str(ix), bid)
                worksheet.write("B" + str(ix), business.company_name)
                worksheet.write("C" + str(ix), business.telephone)
                worksheet.write("D" + str(ix), business.mobile)
                worksheet.write("E" + str(ix), business.email_id)
                if os.path.exists(email_image):
                    worksheet.insert_image("F" + str(ix), email_image)
                worksheet.write("G" + str(ix), reg_time)

                ix += 1

            workbook.close()

            res = {
                "res": True
            }
        else:
            res = {
                "res": False
            }

        return JsonResponse(res)


class BusinessesLoadView(View):

    def get(self, request):

        return HttpResponse("Load Businesses")

    def post(self, request):
        try:
            last_business = Business.objects.order_by('-id')[0]

            last_business_reg_date = last_business.registration_time

            starting_date = last_business_reg_date + datetime.timedelta(days=1)
            starting_date = starting_date.strftime("%d.%m.%Y")
        except Exception as e:
            yesterday = datetime.date.today() - datetime.timedelta(days=1)
            last_day = yesterday - datetime.timedelta(days=7)
            starting_date = last_day.strftime("%d.%m.%Y")

        ending_date = datetime.date.today() - datetime.timedelta(days=1)
        ending_date = ending_date.strftime("%d.%m.%Y")

        if starting_date == ending_date:
            res = {
                "message": f"Businesses List are Up-to-Date. For date range {starting_date}-{ending_date}."
            }
        else:
            res = {
                "message": f"Loading Businesses Started! For date range {starting_date}-{ending_date}."
            }

        load_businesses.delay(starting_date, ending_date)

        return JsonResponse(res)


class CeleryView(View):
    def get(self, request):
        sleepy.delay(10)
        return HttpResponse("Hello Celery")
