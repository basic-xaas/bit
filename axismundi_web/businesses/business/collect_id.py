import os
import time

from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.support.ui import Select


class CollectId:

    def __init__(self, start_date, end_date):
        self.start_date = start_date
        self.end_date = end_date

        # create the driver
        self.driver = self.__create_driver()

    @staticmethod
    def __create_driver():
        driver_path = os.getenv("CHROME_DRIVER_PATH")
        headless = os.getenv("CHROME_HEADLESS")

        options = webdriver.ChromeOptions()
        options.add_argument('--no-sandbox')
        if headless:
            options.add_argument("--headless")
        driver = webdriver.Chrome(driver_path, options=options)

        return driver

    def start(self):

        # open the link for collecting business id
        url = os.getenv("GET_BUSINESSES_ID_URL")
        self.driver.get(url)

        # convert the website into english language
        cvt_english = self.driver.find_element_by_link_text("In English")
        cvt_english.send_keys(u'\ue007')

        # get number of pages available for the range of date
        page_count = self.__get_page_num()
        if page_count is not None:
            # collect the available business id's
            businesses_id = self.__get_businesses_id(page_count)
        else:
            businesses_id = []

        # close the driver
        self.driver.close()

        return businesses_id

    def __get_businesses_id(self, page_count):
        businesses_id = []
        for page in range(1, page_count):
            source = BeautifulSoup(self.driver.page_source)
            table = source.find("table", {"id": "foundPublishedEntries"})

            for row in table.find_all("tr"):
                cells = row.find_all("td")

                if cells:
                    business_id = cells[0].get_text()
                    business_id = self.__clean_text(business_id)

                    reg_time = cells[5].get_text()
                    reg_time = self.__clean_text(reg_time)

                    businesses_id.append([business_id, reg_time])

            pagination = self.driver.find_element_by_class_name("pagination")
            next_page = pagination.find_element_by_link_text(str(page))
            next_page.send_keys(u'\ue007')

        return businesses_id

    def __get_page_num(self):
        field_id = "startDate"
        field = self.driver.find_element_by_id(field_id)
        field.clear()
        field.send_keys(self.start_date)

        field_id = "endDate"
        field = self.driver.find_element_by_id(field_id)
        field.clear()
        field.send_keys(self.end_date)

        select = Select(self.driver.find_element_by_name('registrationTypeCode'))
        select.select_by_value("kltu.U")

        search_button = self.driver.find_element_by_id("_eventId_search")
        search_button.send_keys(u'\ue007')

        time.sleep(2)

        source = BeautifulSoup(self.driver.page_source)

        pagination = source.find("ul", {"class": "pagination"})
        noise = ["«First Page", "‹Previous Page", "…", "›Next Page", "»Last Page"]
        if pagination:
            numbers = [int(page.get_text()) for page in pagination.find_all("li") if page.get_text() not in noise]
            page_count = max(numbers) + 1
        else:
            page_count = None

        return page_count

    @staticmethod
    def __clean_text(text):
        words = text.split("\n")
        words = [word.strip() for word in words if len(word.strip()) > 1]
        text = " ".join(words)

        return text
