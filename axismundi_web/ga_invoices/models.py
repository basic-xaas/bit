from datetime import datetime
from django.db import models


class GAAccount(models.Model):
    account_name = models.CharField(max_length=128, blank=False)
    account_site_url = models.URLField(max_length=128, blank=False)
    account_email_id = models.EmailField(max_length=60, blank=False)
    ga_view_id = models.CharField(unique=True, max_length=15, blank=False)
    amount_per_session = models.FloatField(default=10.00)
    vat = models.FloatField(default=24.00)
    timestamp = models.DateTimeField(default=datetime.now, blank=True)

    def __str__(self):
        return self.account_name


class InvoiceHistory(models.Model):
    profile_id = models.ForeignKey(GAAccount, on_delete=models.CASCADE)
    invoice_path = models.CharField(max_length=250, blank=False)
    mail_send_to = models.EmailField(max_length=60, blank=True)
    mail_sent = models.BooleanField(default=False)
    timestamp = models.DateTimeField(default=datetime.now, blank=True)
