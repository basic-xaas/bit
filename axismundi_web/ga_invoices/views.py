from django.views import View
from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin

from .invoices import Invoices
from .tasks import daily_invoices
from .models import GAAccount, InvoiceHistory
from .serializers import AccountSerializer


class IndexView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):
        histories = InvoiceHistory.objects.all().order_by('-timestamp')

        context = {
            "histories": histories
        }

        return render(request, 'ga_invoices/index.html', context)


class AccountView(APIView):

    def get(self, request):
        accounts = GAAccount.objects.all()
        serializer = AccountSerializer(accounts, many=True)

        return Response(serializer.data)

    def post(self, request):
        serializer = AccountSerializer(data=request.data)

        if serializer.is_valid():
            serializer.save()
            res = serializer.data
        else:
            res = serializer.errors

        return Response(res)
