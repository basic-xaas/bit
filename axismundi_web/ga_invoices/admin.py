from django.contrib import admin

from .models import *

admin.site.register(GAAccount)
admin.site.register(InvoiceHistory)