from celery import shared_task
from .models import GAAccount
from .invoices import Invoices


@shared_task()
def daily_invoices():
    profiles = GAAccount.objects.all()
    invoices = Invoices(profiles)
    invoices.send_daily_mail()
