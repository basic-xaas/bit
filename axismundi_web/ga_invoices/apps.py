from django.apps import AppConfig


class GaInvoicesConfig(AppConfig):
    name = 'ga_invoices'
