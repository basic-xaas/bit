import os
import calendar
import datetime

import pandas as pd

from reportlab.pdfgen import canvas
from reportlab.pdfbase import pdfmetrics
from reportlab.pdfbase.ttfonts import TTFont
from reportlab.lib import colors

from django.conf import settings
from django.contrib.staticfiles.storage import staticfiles_storage


def get_sessions_bill(details, company):
    session = details["ga:sessions"]
    session_count = int(session)

    if session_count <= 100:
        session_price = 0.20
    elif 100 < session_count <= 200:
        session_price = 0.19
    elif 200 < session_count <= 300:
        session_price = 0.18
    elif 300 < session_count <= 400:
        session_price = 0.17
    elif 400 < session_count <= 500:
        session_price = 0.16
    elif 500 < session_count <= 750:
        session_price = 0.15
    elif 750 < session_count <= 1000:
        session_price = 0.14
    elif 1000 < session_count <= 1500:
        session_price = 0.13
    elif 1500 < session_count <= 2000:
        session_price = 0.12
    else:
        session_price = 0.11

    session_bill = round(session_price * session_count, 2)

    if session_bill < 10:
        session_bill = round(float(company.amount_per_session), 2)

    return session_bill


def add_header(pdf):
    title_font = "Verdana"

    # 1.1 Add Company Logo
    logo_path = os.path.join(os.getcwd(), 'static', 'admin_portal', 'images', 'icons', "logo.jpeg")
    pdf.drawImage(logo_path, 80, 730, width=90, height=90)

    # 1.2 Add Company Name
    pdf.setFont(title_font, 10)
    pdf.drawString(300, 795, "End Of Day Solutions Oy")
    pdf.drawString(300, 780, "Sopenkorvenkatu 9, 15800 LAHTI, Finland")
    pdf.drawString(300, 765, "VAT-code: FI28466216")
    pdf.setFillColor(colors.blue)
    pdf.drawString(300, 740, "basic.international")
    pdf.linkURL(url="https://basic.international", rect=(400, 745, 300, 740))
    pdf.setFillColor(colors.black)

    # 1.3 Add Separator Lines
    pdf.lines([(193, 715, 60, 715), (283, 715, 198, 715), (550, 715, 288, 715)])


def add_footer(pdf):
    normal_font = "Courier"

    pdf.setFont(normal_font, 10)

    pdf.setFillColor(colors.blue)
    pdf.drawString(263, 90, "info@eods.it")
    pdf.linkURL(url="mailto:info@eods.it", rect=(400, 745, 300, 740))
    pdf.drawString(267, 75, "www.eods.it")
    pdf.linkURL(url="www.eods.it", rect=(400, 745, 300, 740))
    pdf.setFillColor(colors.black)
    pdf.drawString(260, 60, "+358400137147")


def create_analytics_report(data, company, storage_path, date_range):

    pdf_name = "invoice.pdf"
    # month_name = calendar.month_name[month_num]

    store_path = storage_path
    pdf_path = os.path.join(store_path, pdf_name)

    # Initialize the different fonts
    font_path = os.path.join(os.getcwd(), 'static', 'admin_portal', 'fonts', 'Verdana.ttf')
    pdfmetrics.registerFont(TTFont('Verdana', font_path))

    normal_font = "Courier"
    bold_font = "Courier-Bold"

    # Initialize PDF
    pdf = canvas.Canvas(pdf_path)
    pdf.setTitle(company.account_name)

    # Page 1 - Add the Report Data and Bill Amount
    # 1.1 Add Header
    add_header(pdf)

    # 1.2 Add Report Name
    pdf.setFont(bold_font, 11)
    pdf.drawString(60, 690, company.account_name + ' - ' + str(date_range))

    # 1.2 Google Analytics Report
    pdf.setFont(normal_font, 10)
    pdf.drawString(60, 665, 'Google Analytics Report')
    pdf.line(60, 660, 200, 660)

    # Adding Titles
    pdf.drawString(60, 640, 'Users:')
    pdf.drawString(60, 620, 'New Users:')
    pdf.drawString(60, 600, 'Sessions:')
    pdf.drawString(60, 580, 'Number of Session per User:')
    pdf.drawString(60, 560, 'Page Views:')
    pdf.drawString(60, 540, 'Page / Session:')
    pdf.drawString(60, 520, 'Bounce Rate:')
    pdf.drawString(60, 500, 'Avg. Session Duration:')

    # Add values
    users = data["ga:users"]
    new_users = data["ga:newUsers"]
    sessions = data["ga:sessions"]
    sessions_per_user = str(round(float(data["ga:sessionsPerUser"]), 2))
    page_views = data["ga:pageviews"]
    page_views_per_session = str(round(float(data["ga:pageviewsPerSession"]), 2))
    avg_session_duration = str(datetime.timedelta(seconds=int(float(data["ga:avgSessionDuration"]))))
    bounce_rate = str(round(float(data["ga:bounceRate"]), 2)) + "%"

    pdf.drawString(250, 640, users)
    pdf.drawString(250, 620, new_users)
    pdf.drawString(250, 600, sessions)
    pdf.drawString(250, 580, sessions_per_user)
    pdf.drawString(250, 560, page_views)
    pdf.drawString(250, 540, page_views_per_session)
    pdf.drawString(250, 520, bounce_rate)
    pdf.drawString(250, 500, avg_session_duration)

    # 1.3 Adding Conversions Details
    # pdf.drawString(60, 450, config["google"]["title"] + ' Conversions')
    # pdf.line(60, 446, 195, 446)
    #
    # pdf.drawString(60, 425, 'Email Clicks:')
    # pdf.drawString(60, 405, 'Phone Clicks:')
    # pdf.drawString(60, 385, 'Address Clicks:')

    # csv_path = os.path.join(store_path, 'contact_logs.csv')
    # counts = pd.read_csv(csv_path)
    # email_count = str(counts["email_clicks_count"].sum())
    # phone_count = str(counts["phone_clicks_count"].sum())
    # address_count = str(counts["address_clicks_count"].sum())

    # pdf.drawString(250, 425, email_count)
    # pdf.drawString(250, 405, phone_count)
    # pdf.drawString(250, 385, address_count)

    # 1.4 Add Bill Details
    pdf.drawString(60, 335, 'Bill Details')
    pdf.line(60, 332, 135, 332)

    pdf.drawString(60, 310, 'Bill Amount:')
    pdf.drawString(60, 290, '+VAT (24%):')
    pdf.drawString(60, 270, 'Total Bill Amounts:')

    bill_amount = get_sessions_bill(data, company)
    vat_amount = round(float(company.vat/100)*bill_amount, 2)
    total_bill = round((bill_amount + vat_amount), 2)

    pdf.drawString(250, 310, str(format(bill_amount, '.2f'))+" \u20ac")
    pdf.drawString(250, 290, str(format(vat_amount, '.2f'))+" \u20ac")
    pdf.drawString(250, 270, str(format(total_bill, '.2f'))+" \u20ac")

    # 1.5 Add Footer
    add_footer(pdf)

    # Create New Page
    pdf.showPage()
    # End of Page 1

    # Page 2 - Add the Graph Details of Sessions
    # 2.1 Add Header
    add_header(pdf)

    # 2.3 Adding graph: sessions per day
    pdf.drawImage(os.path.join(store_path, 'sessions_per_day.png'), 60, 500, width=450, height=180)
    pdf.drawImage(os.path.join(store_path, 'sessions_per_country.png'), 60, 130, width=450, height=330)

    # 2.2 Google Analytics Session Details
    pdf.setFont(normal_font, 10)
    pdf.drawString(60, 665, 'Sessions Per Day')
    pdf.line(60, 660, 158, 660)

    pdf.drawString(60, 455, 'Sessions Per Country')
    pdf.line(60, 450, 183, 450)

    # 2.4 Add Footer
    add_footer(pdf)

    # Create New Page
    pdf.showPage()
    # End of Page 2

    # Page 3 - Add the Graph Details of Users
    # 3.1 Add Header
    add_header(pdf)

    # 3.3 Adding graph: users per day
    pdf.drawImage(os.path.join(store_path, 'users_per_day.png'), 60, 500, width=450, height=180)
    pdf.drawImage(os.path.join(store_path, 'users_per_country.png'), 60, 130, width=450, height=330)

    # 3.2 Google Analytics User Details
    pdf.setFont(normal_font, 10)
    pdf.drawString(60, 665, 'Users Per Day')
    pdf.line(60, 660, 140, 660)

    pdf.drawString(60, 455, 'Users Per Country')
    pdf.line(60, 450, 165, 450)

    # 2.4 Add Footer
    add_footer(pdf)

    # Create New Page
    pdf.showPage()
    # End of Page 3

    # Page 4 - Add the Graph Details of Finland
    # 4.1 Add Header
    add_header(pdf)

    # 4.3 Adding graph: users per day
    pdf.drawImage(os.path.join(store_path, 'users_per_city.png'), 60, 90, width=450, height=330)
    pdf.drawImage(os.path.join(store_path, 'sessions_per_city.png'), 60, 375, width=450, height=330)

    # 4.2 Google Analytics User Details
    pdf.setFont(normal_font, 10)
    pdf.drawString(60, 665, 'Sessions Per City (Finland)')
    pdf.line(60, 660, 223, 660)

    pdf.drawString(60, 385, 'Users Per City (Finland)')
    pdf.line(60, 380, 205, 380)

    # 4.4 Add Footer
    add_footer(pdf)

    # Final - Save the PDF
    pdf.save()

    return pdf_path
