import os

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

from rest_framework import serializers
from .models import GAAccount


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = GAAccount
        fields = "__all__"

    def validate_ga_view_id(self, value):
        scopes = [os.getenv("ANALYTICS_SCOPE")]
        key_file_location = 'client_secrets.json'
        api_name = 'analytics'
        api_version = 'v3'

        credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scopes)
        service = build(api_name, api_version, credentials=credentials)

        try:
            service.data().ga().get(
                ids='ga:' + value,
                start_date='7daysAgo',
                end_date='today',
                metrics='ga:sessions').execute()
        except Exception as e:
                raise serializers.ValidationError("Invalid Google Analytics View ID")

        return value
