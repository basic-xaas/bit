from django.urls import path

from . import views

app_name = 'ga_invoices'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('accounts', views.AccountView.as_view(), name='company_register'),
]
