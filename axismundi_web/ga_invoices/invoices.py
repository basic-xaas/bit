import os
import datetime

from django.conf import settings
from django.core.mail import EmailMessage

from .models import InvoiceHistory

from .src import GoogleAnalyticsData, create_invoice


class Invoices:

    def __init__(self, profiles):
        self.profiles = profiles
        invoices_path = os.path.join(settings.MEDIA_ROOT, 'invoices')
        if not os.path.exists(invoices_path):
            os.makedirs(invoices_path)
        self.invoices_path = invoices_path

    def send_daily_mail(self):
        list(map(self.send_mail, self.profiles))

    def send_mail(self, profile):
        # Step 1: Check and Prepare Folder for the Company
        company_path = os.path.join(self.invoices_path, profile.ga_view_id)
        if not os.path.exists(company_path):
            os.makedirs(company_path)

        # Step 2: Require the Date Range
        invoice_histories = InvoiceHistory.objects.filter(profile_id=profile.id).order_by("-timestamp")
        if len(invoice_histories):
            last_invoice_date = invoice_histories[0].timestamp
        else:
            last_invoice_date = profile.timestamp   # - datetime.timedelta(days=30)

        today = datetime.datetime.now()
        day_diff = today.date() - last_invoice_date.date()
        day_count = day_diff.days

        print(f"{profile.account_name} ({profile.ga_view_id}), Days Diff: {day_count}")
        print(f"Last Time Invoice Sent: {last_invoice_date}")
        if day_count >= 30:

            # Step 3: Create Current Invoice Folder
            month_folder = os.path.join(company_path, today.strftime("%Y-%m-%d"))
            if not os.path.exists(month_folder):
                os.makedirs(month_folder)

            # Step 4: Design starting and Ending date for collecting GA Data
            start_date = last_invoice_date.strftime('%Y-%m-%d')
            end_date = today.strftime('%Y-%m-%d')

            # Step 5: Create invoices for last 30 days
            analytics_data = GoogleAnalyticsData(
                view_id=profile.ga_view_id,
                start_date=start_date,
                end_date=end_date,
                storage_path=month_folder
            ).collect()

            # Step 6: Create Invoice
            date_range = f"{last_invoice_date.strftime('%Y/%m/%d')} to {today.strftime('%Y/%m/%d')}"
            invoice_path = create_invoice(analytics_data, profile, month_folder, date_range)

            invoice_url = "/".join(invoice_path.split("/")[-4:])
            invoice_url = os.path.join(settings.MEDIA_URL, invoice_url)
            # Step 7: Send Invoice via Mail
            try:
                from_email = settings.EMAIL_HOST_USER
                to_email = [profile.account_email_id]

                message = EmailMessage(
                    'BASIC SaaS Invoice',
                    '',
                    from_email,
                    to_email
                )
                message.attach_file(invoice_path)
                message.send(fail_silently=True)

                history_invoice = InvoiceHistory(
                    profile_id=profile,
                    invoice_path=invoice_url,
                    mail_send_to=to_email[0],
                    mail_sent=True
                )
                message = "Invoice Sent Successfully"
            except Exception as e:
                history_invoice = InvoiceHistory(
                    profile_id=profile,
                    invoice_path=invoice_url,
                    mail_sent=False
                )
                message = "Error in Sending Mail"

            history_invoice.save()
            print(message)
