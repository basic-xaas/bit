import os
import shutil
import calendar

from datetime import datetime
from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials

from django.views import View
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from django.core.mail import EmailMessage
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404
from django.contrib.sites.shortcuts import get_current_site
from django.core.files.storage import default_storage

from .src import GoogleAnalyticsData, create_invoice
from .models import *


class IndexView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):

        try:
            profiles = get_list_or_404(AnalyticsView)
        except Exception as e:
            profiles = []

        try:
            companies_list = get_list_or_404(AnalyticsCompany)

            invoices_path = os.path.join(settings.MEDIA_ROOT, 'invoices')

            companies = []
            for idx, company in enumerate(companies_list):
                company_path = os.path.join(invoices_path, company.profile.view_id)

                recent_invoice_name = False
                recent_invoice_path = False

                if os.path.exists(company_path):
                    month_folders = os.listdir(company_path)
                    if len(month_folders):
                        recent_month = month_folders[::-1][0]
                        pdf_path = os.path.join(
                            settings.MEDIA_URL, "invoices", str(company.profile.view_id), recent_month, 'invoice.pdf'
                        )

                        month = recent_month.split(" ")
                        month[1] = calendar.month_name[int(month[1])]
                        invoice = " ".join(month[::-1])

                        recent_invoice_name = invoice
                        recent_invoice_path = pdf_path

                companies.append({
                    "id": company.id,
                    "company_site": company.company_site,
                    "company_name": company.company_name,
                    "recent_invoice_name": recent_invoice_name,
                    "recent_invoice_path": recent_invoice_path,
                })
        except Exception as e:
            companies = []

        context = {
            "profiles": profiles,
            "companies": companies
        }

        return render(request, 'invoices/index.html', context)

    def post(self, request):
        profile = AnalyticsView.objects.filter(view_id=request.POST["analytics_id"]).first()

        try:
            company = AnalyticsCompany.objects.get(
                profile=profile
            )
            company.company_name = request.POST["company_name"]
            company.company_site = request.POST["company_site"]
            company.company_saas_url = request.POST["saas_url"]
            company.amount_per_session = request.POST["amount_per_session"]
            company.vat = request.POST["vat"]

        except AnalyticsCompany.DoesNotExist:
            company = AnalyticsCompany(
                profile=profile,
                company_name=request.POST["company_name"],
                company_site=request.POST["company_site"],
                company_saas_url=request.POST["saas_url"],
                amount_per_session=request.POST["amount_per_session"],
                vat=request.POST["vat"]
            )

        company.save()

        return redirect('invoices:index')


class InvoicesView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request, company_id=None):

        company = AnalyticsCompany.objects.filter(id=company_id).first()

        invoices_path = os.path.join(settings.MEDIA_ROOT, 'invoices')
        if not os.path.exists(invoices_path):
            os.makedirs(invoices_path)

        company_path = os.path.join(invoices_path, company.profile.view_id)
        if not os.path.exists(company_path):
            os.makedirs(company_path)

        month_folders = os.listdir(company_path)
        month_folders = month_folders[::-1]
        invoices = []
        if len(month_folders):
            for ix, month in enumerate(month_folders):
                pdf_path = os.path.join(settings.MEDIA_URL, "invoices", str(company.profile.view_id), month_folders[ix], 'invoice.pdf')

                month = month.split(" ")
                month[1] = calendar.month_name[int(month[1])]
                invoice = " ".join(month[::-1])
                invoices.append({
                        "name": invoice,
                        "path": pdf_path
                    })

        context = {
            "company": company,
            "invoices": invoices
        }

        return render(request, 'invoices/invoices.html', context)

    def post(self, request, company_id=None):
        company = AnalyticsCompany.objects.filter(id=company_id).first()
        profile_id = company.profile.view_id

        # check for invoices folder
        invoices_path = os.path.join(settings.MEDIA_ROOT, 'invoices')
        if not os.path.exists(invoices_path):
            os.makedirs(invoices_path)

        # check company folder under invoices folder
        company_path = os.path.join(invoices_path, company.profile.view_id)
        if not os.path.exists(company_path):
            os.makedirs(company_path)

        # collect date range
        month_num = int(request.POST["month"])
        year = int(request.POST["year"])
        last_date = calendar.monthrange(year, month_num)[1]

        # make the starting and ending date
        start_date = datetime.strptime(f"1-{month_num}-{year}", '%d-%m-%Y').strftime('%Y-%m-%d')
        end_date = datetime.strptime(f"{last_date}-{month_num}-{year}", '%d-%m-%Y').strftime('%Y-%m-%d')

        # check for month folder
        month_folder = os.path.join(company_path, f"{year} {month_num}")
        if not os.path.exists(month_folder):
            os.makedirs(month_folder)

        # collect data from google analytics
        analytics_data = GoogleAnalyticsData(
            view_id=profile_id,
            start_date=start_date,
            end_date=end_date,
            storage_path=month_folder
        ).collect()

        create_invoice(analytics_data, company, month_folder, month_num, year)

        return redirect('invoices:invoices_view', company.id)


class AnalyticsAccountsView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):

        try:
            accounts = get_list_or_404(AnalyticsAccount)
        except Exception as e:
            print("Error in collecting Analytics Accounts details,", e)
            accounts = []

        context = {
            "accounts": accounts
        }

        return render(request, 'invoices/accounts.html', context=context)


class RefreshAccountsView(View):

    def get(self, request):
        scopes = [os.getenv("ANALYTICS_SCOPE")]
        key_file_location = 'client_secrets.json'
        api_name = 'analytics',
        api_version = 'v3',

        # Get a service that communicates to a Google API
        credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scopes)
        # Build the service object.
        service = build(api_name, api_version, credentials=credentials)

        # Get a list of all Google Analytics accounts for this user
        accounts = service.management().accounts().list().execute()

        for account in accounts.get("items"):
            if account:
                try:
                    account_db = AnalyticsAccount.objects.get(
                        account_id=account.get('id')
                    )
                except AnalyticsAccount.DoesNotExist:
                    account_db = AnalyticsAccount(
                        account_id=account.get('id'),
                        account_self_link=account.get('selfLink'),
                        account_name=account.get('name'),
                    )
                # else:
                #     account_db.account_self_link = account.get('selfLink')
                #     account_db.account_name = account.get('name')

                account_db.save()

        return redirect('invoices:analytics_accounts')


class AnalyticsProperties(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request, account_id=None):
        if account_id is None:
            return redirect('invoices:analytics_accounts')

        account = AnalyticsAccount.objects.filter(account_id=account_id).first()

        try:
            properties = get_list_or_404(AnalyticsProperty, account_id=account)
        except Exception as e:
            print("Error in collecting Analytics Accounts details,", e)
            properties = []

        context = {
            "account": account,
            "properties": properties
        }

        return render(request, 'invoices/properties.html', context=context)


class RefreshPropertiesView(View):

    def get(self, request, account_id=None):
        if account_id is None:
            return redirect('invoices:analytics_accounts')

        scopes = [os.getenv("ANALYTICS_SCOPE")]
        key_file_location = 'client_secrets.json'
        api_name = 'analytics',
        api_version = 'v3',

        # Get a service that communicates to a Google API
        credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scopes)
        # Build the service object.
        service = build(api_name, api_version, credentials=credentials)

        # Get a list of all Google Analytics properties for this user for account_id
        properties = service.management().webproperties().list(
            accountId=account_id).execute()

        for prop in properties.get("items"):
            if prop:
                try:
                    property_db = AnalyticsProperty.objects.get(
                        property_id=prop.get('id')
                    )
                except AnalyticsProperty.DoesNotExist:
                    property_db = AnalyticsProperty(
                        account=AnalyticsAccount.objects.get(account_id=account_id),
                        property_id=prop.get('id'),
                        property_self_link=prop.get('selfLink'),
                        property_name=prop.get('name'),
                    )

                property_db.save()

        return redirect('invoices:analytics_properties', account_id)


class AnalyticsViewsView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request, property_id=None):
        if property_id is None:
            return redirect('invoices:analytics_accounts')

        prop = AnalyticsProperty.objects.filter(property_id=property_id).first()
        account = prop.account

        try:
            views = get_list_or_404(AnalyticsView, property=prop)
        except Exception as e:
            print("Error in collecting Analytics View details,", e)
            views = []

        context = {
            "account": account,
            "property": prop,
            "views": views
        }

        return render(request, 'invoices/views.html', context=context)


class RefreshViewsView(View):

    def get(self, request, property_id=None):
        if property_id is None:
            return redirect('invoices:analytics_accounts')

        scopes = [os.getenv("ANALYTICS_SCOPE")]
        key_file_location = 'client_secrets.json'
        api_name = 'analytics',
        api_version = 'v3',

        # Get a service that communicates to a Google API
        credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scopes)
        # Build the service object.
        service = build(api_name, api_version, credentials=credentials)

        # Get a list of all Google Analytics properties for this user for account_id
        prop = AnalyticsProperty.objects.filter(property_id=property_id).first()
        account_id = prop.account.account_id
        property_id = prop.property_id
        profiles = service.management().profiles().list(
            accountId=account_id,
            webPropertyId=property_id).execute()

        for profile in profiles.get("items"):
            if profile:
                try:
                    view_db = AnalyticsView.objects.get(
                        view_id=profile.get('id')
                    )
                except AnalyticsView.DoesNotExist:
                    view_db = AnalyticsView(
                        property=AnalyticsProperty.objects.get(property_id=property_id),
                        view_id=profile.get('id'),
                        view_self_link=profile.get('selfLink'),
                        view_name=profile.get('name'),
                    )
                # else:
                #     property_db.account_self_link = profile.get('selfLink')
                #     property_db.account_name = profile.get('name')

                view_db.save()

        return redirect('invoices:analytics_views', property_id)


class InvoiceMailView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):
        return HttpResponse("Call POST to download Send Invoice via Mail")

    def post(self, request):
        data = request.POST.dict()

        try:
            company = AnalyticsCompany.objects.filter(id=data["company_id"]).first()

            invoices_path = os.path.join(settings.MEDIA_ROOT, 'invoices')
            company_path = os.path.join(invoices_path, company.profile.view_id)

            month_folders = os.listdir(company_path)
            recent_month = month_folders[::-1][0]
            recent_invoice_path = os.path.join(
                settings.MEDIA_URL, "invoices", str(company.profile.view_id), recent_month, 'invoice.pdf'
            )

            month = recent_month.split(" ")
            month[1] = calendar.month_name[int(month[1])]
            recent_invoice_name = " ".join(month[::-1]) + ".pdf"

            attachment_path = os.getcwd() + recent_invoice_path

            from_email = settings.EMAIL_HOST_USER
            to_email = ["paragsharma.eods@gmail.com"]

            message = EmailMessage(
                'Subject',
                'Body goes here',
                from_email,
                to_email
            )
            message.attach_file(attachment_path)
            message.send(fail_silently=True)

            res = {
                "res": True
            }
        except Exception as e:
            res = {
                "res": False
            }

        return JsonResponse(res)
