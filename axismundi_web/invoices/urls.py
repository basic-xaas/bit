from django.urls import path

from . import views

app_name = 'invoices'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('view/<int:company_id>', views.InvoicesView.as_view(), name='invoices_view'),
    path('analytics-accounts', views.AnalyticsAccountsView.as_view(), name="analytics_accounts"),
    path('analytics-property/<int:account_id>', views.AnalyticsProperties.as_view(),
         name="analytics_properties"),
    path('analytics-views/<str:property_id>', views.AnalyticsViewsView.as_view(), name="analytics_views"),
    path('refresh-analytics-accounts', views.RefreshAccountsView.as_view(), name="refresh_accounts"),
    path('refresh-analytics-properties/<int:account_id>', views.RefreshPropertiesView.as_view(),
         name="refresh_properties"),
    path('refresh-analytics-views/<str:property_id>', views.RefreshViewsView.as_view(),
         name="refresh_views"),
    path('send_invoice_mail', views.InvoiceMailView.as_view(), name='invoice_mail')
]
