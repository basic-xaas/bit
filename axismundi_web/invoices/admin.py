from django.contrib import admin

# Register your models here.
from .models import *

admin.site.register(AnalyticsAccount)
admin.site.register(AnalyticsProperty)
admin.site.register(AnalyticsView)
admin.site.register(AnalyticsCompany)
