import os
import random

import pandas as pd
import plotly.graph_objects as go

from apiclient.discovery import build
from oauth2client.service_account import ServiceAccountCredentials


class GoogleAnalyticsData:

    def __init__(self, view_id, start_date, end_date, storage_path):
        self.view_id = view_id
        self.start_date = start_date
        self.end_date = end_date
        self.storage_path = storage_path
        self.service = self.__connection()

    def collect(self):
        # get all details
        data = self.__get_details()

        plot = ViewData()
        store_path = self.storage_path

        # get date wise data
        date_wise_data = self.__get_per_day_data()

        # get users per day
        date_cols = date_wise_data.columns
        user_data = date_wise_data.copy()
        session_data = date_wise_data.copy()

        params = ["number of users per day", "date", "users count"]
        user_data[date_cols[0]] = user_data[date_cols[0]] + "(" + user_data[date_cols[1]] + ")"
        date = user_data[date_cols[0]]
        users = user_data[date_cols[1]]
        figure = plot.scatter_plot(date, users, params)
        figure.write_image(os.path.join(store_path, 'users_per_day.png'))

        # session per day
        params = ["number of sessions per day", "date", "session count"]
        session_data[date_cols[0]] = session_data[date_cols[0]] + "(" + session_data[date_cols[2]] + ")"
        date = session_data[date_cols[0]]
        countries = session_data[date_cols[2]]
        figure = plot.scatter_plot(date, countries, params)
        figure.write_image(os.path.join(store_path, 'sessions_per_day.png'))

        # get country and city wise data
        country_wise_data = self.__get_country_city_data()

        # country data creation
        cols = country_wise_data.columns
        country_group = country_wise_data.groupby(cols[0])
        unq_country = country_wise_data[cols[0]].unique()
        record = []
        for country in unq_country:
            c_data = country_group.get_group(country)
            c_data[cols[2]] = c_data[cols[2]].astype(int)
            c_data[cols[3]] = c_data[cols[3]].astype(int)
            record.append([country, c_data[cols[2]].sum(), c_data[cols[3]].sum()])

        country_data = pd.DataFrame(data=record, columns=[cols[0], cols[2], cols[3]])
        country_data = country_data.astype(str)

        country_cols = country_data.columns
        user_data = country_data.copy()
        session_data = country_data.copy()

        # user per country
        params = ["number of users per country", "country", "user count"]
        user_data[country_cols[0]] = user_data[country_cols[0]] + "(" + user_data[country_cols[1]] + ")"
        country = user_data[country_cols[0]]
        users = user_data[country_cols[1]]
        figure = plot.bar_plot(country, users, params)
        figure.write_image(os.path.join(store_path, 'users_per_country.png'))

        # session per country
        params = ["number of sessions per country", "country", "session count"]
        session_data[country_cols[0]] = session_data[country_cols[0]] + "(" + session_data[country_cols[2]] + ")"
        country = session_data[country_cols[0]]
        sessions = session_data[country_cols[2]]
        figure = plot.bar_plot(country, sessions, params)
        figure.write_image(os.path.join(store_path, 'sessions_per_country.png'))

        # Finland Data
        city_data = country_wise_data[country_wise_data[cols[0]] == "Finland"]
        user_data = city_data.copy()
        session_data = city_data.copy()

        # users per city
        params = ["number of users per city", "city", "user count"]
        user_data[cols[1]] = user_data[cols[1]] + "(" + user_data[cols[2]] + ")"
        city = user_data[cols[1]]
        users = user_data[cols[2]]
        figure = plot.bar_plot(city, users, params)
        figure.write_image(os.path.join(store_path, 'users_per_city.png'))

        # sessions per city
        params = ["number of sessions per city", "city", "sessions count"]
        session_data[cols[1]] = session_data[cols[1]] + "(" + session_data[cols[3]] + ")"
        city = user_data[cols[1]]
        sessions = user_data[cols[3]]
        figure = plot.bar_plot(city, sessions, params)
        figure.write_image(os.path.join(store_path, 'sessions_per_city.png'))

        return data

    def __get_details(self):
        details = self.service.data().ga().get(
            ids='ga:' + self.view_id,
            start_date=self.start_date,
            end_date=self.end_date,
            metrics='ga:users,ga:newUsers,ga:sessions,ga:percentNewSessions,ga:sessionsPerUser,ga:pageviews,'
                    'ga:pageviewsPerSession,ga:avgSessionDuration,ga:bounceRate').execute()

        return details["totalsForAllResults"]

    def __get_per_day_data(self):
        details = self.service.data().ga().get(
            ids='ga:' + self.view_id,
            start_date=self.start_date,
            end_date=self.end_date,
            metrics='ga:users,ga:sessions',
            dimensions='ga:date'
        ).execute()

        header_details = details["columnHeaders"]
        header = [head["name"].split(":")[1] for head in header_details]
        rows = details["rows"]
        df = pd.DataFrame(data=rows, columns=header)

        return df

    def __get_country_city_data(self):
        details = self.service.data().ga().get(
            ids='ga:' + self.view_id,
            start_date=self.start_date,
            end_date=self.end_date,
            metrics='ga:users,ga:sessions',
            dimensions='ga:country,ga:city'
        ).execute()

        header_details = details["columnHeaders"]
        header = [head["name"].split(":")[1] for head in header_details]
        rows = details["rows"]
        df = pd.DataFrame(data=rows, columns=header)

        return df

    @staticmethod
    def __connection():
        scopes = [os.getenv("ANALYTICS_SCOPE")]
        key_file_location = 'client_secrets.json'
        api_name = 'analytics',
        api_version = 'v3',
        # Get a service that communicates to a Google API
        credentials = ServiceAccountCredentials.from_json_keyfile_name(key_file_location, scopes=scopes)
        # Build the service object.
        service = build(api_name, api_version, credentials=credentials)

        return service


class ViewData:

    @staticmethod
    def scatter_plot(x, y, params):

        fig = go.Figure()
        fig.add_trace(go.Scatter(
            x=x,
            y=y,
            mode='lines+markers'
        ))
        fig.update_layout(
            height=350,
            width=900,
            xaxis_title=params[1],
            yaxis_title=params[2]
        )
        fig.update_xaxes(nticks=15)
        fig.update_yaxes(nticks=10)

        return fig

    @staticmethod
    def bar_plot(x, y, params):

        fig = go.Figure()
        fig.add_trace(go.Bar(
            x=x,
            y=y
        ))
        fig.update_layout(
            height=600,
            width=900,
            xaxis_title=params[1],
            yaxis_title=params[2]
        )

        return fig
