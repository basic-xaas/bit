from django.db import models


class AnalyticsAccount(models.Model):
    account_id = models.CharField(unique=True, max_length=15)
    account_self_link = models.URLField(max_length=200)
    account_name = models.CharField(max_length=100)

    def __str__(self):
        return self.account_name


class AnalyticsProperty(models.Model):
    account = models.ForeignKey(AnalyticsAccount, on_delete=models.CASCADE)
    property_id = models.CharField(unique=True, max_length=15)
    property_self_link = models.URLField(max_length=400)
    property_name = models.CharField(max_length=100)

    def __str__(self):
        return self.property_name


class AnalyticsView(models.Model):
    property = models.ForeignKey(AnalyticsProperty, on_delete=models.CASCADE)
    view_id = models.CharField(unique=True, max_length=15)
    view_self_link = models.URLField(max_length=400)
    view_name = models.CharField(max_length=100)

    def __str__(self):
        return self.view_name


class AnalyticsCompany(models.Model):
    profile = models.OneToOneField(AnalyticsView, on_delete=models.CASCADE)
    company_name = models.CharField(max_length=100)
    company_site = models.URLField(max_length=100)
    company_saas_url = models.URLField(max_length=100)
    amount_per_session = models.FloatField(default=10.00)
    vat = models.FloatField(default=24.00)

    def __str__(self):
        return self.company_name
