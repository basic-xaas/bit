from django.urls import path
from django.views.generic import TemplateView

from . import views

app_name = 'admin_portal'

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('admin_panel/login', views.LoginView.as_view(), name="admin_login"),
    path('admin_panel/logout', views.LogOutView.as_view(), name="admin_logout")
]
