from django.views import View
from django.http import HttpResponse
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from django.shortcuts import render, redirect, get_object_or_404, get_list_or_404


from .models import *


class IndexView(LoginRequiredMixin, UserPassesTestMixin, View):
    login_url = '/admin_panel/login'
    redirect_field_name = "redirect_to"

    def test_func(self):
        return self.request.user.is_superuser or self.request.user.is_staff

    def get(self, request):

        return render(request, 'admin_portal/index.html')


class LoginView(View):

    def get(self, request):

        if request.user.is_authenticated:
            return redirect('admin_portal:index')

        return render(request, 'admin_portal/login.html')

    def post(self, request):
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)

        context = {}
        if user is not None:
            login(request, user)
            return redirect("admin_portal:index")
        else:
            context["error"] = "Invalid Admin Credentials"

        return render(request, 'admin_portal/login.html', context)


class LogOutView(View):

    def get(self, request):
        logout(request)
        return redirect('admin_portal:admin_login')
