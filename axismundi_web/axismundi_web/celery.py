import os

from celery import Celery
from celery.schedules import crontab

# from businesses.tasks import load_businesses_daily


# set the default Django settings module for the 'celery' program.
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'axismundi_web.settings')

app = Celery('axismundi_web')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django app configs.
app.autodiscover_tasks()

app.conf.beat_schedule = {
    'load-businesses-daily': {
        'task': 'businesses.tasks.load_businesses_daily',
        'schedule': crontab(minute=0, hour=0)
    },
    'send_invoices_daily_check': {
        'task': 'ga_invoices.tasks.daily_invoices',
        'schedule': crontab(minute=0, hour=0)
    }
}


@app.task(bind=True)
def debug_task(self):
    print(f'Request: {self.request!r}')