from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('admin_portal.urls')),
    path('businesses/', include('businesses.urls')),
    path('invoices/', include('invoices.urls')),
    path('ga-invoices/', include('ga_invoices.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
